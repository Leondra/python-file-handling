def definitions():
    print("\nHere's some info:\n"
          "\"r\": Read. Opens a file to read, error if file doesn't exist.\n"
          "\"a\": Append. Opens a file to append, creates if file doesn't exist.\n"
          "\"w\": Write. Opens a file for writing, creates if file doesn't exist.\n"
          "\"x\": Create. Creates a file, error if file exists.")
    return

def read():
    try:
        file = open(readfile, "r")
        print("These are the contents of the file:\n")
        print("\t" + file.read())
        file.close()
    except:
        print("This file does not exist.")
    return

def append():
    try:
        file = open(readfile, "a")
        print("What do you want to append? ", end="")
        content = input()
        file.write(content)
        file.close()
    except:
        print("This file does not exist.")
        return
    print("Would you like to read the newly appended file? [Y/N] ", end="")
    readcommand = input()
    if readcommand == "Y": read()
    elif readcommand == "N": return
    else:
        "Incorrect input, returning."
    return

def write():
    try:
        file = open(readfile, "w")
        print("What do you want to write? ", end="")
        content = input()
        file.write(content)
        file.close()
    except:
        print("This file does not exist.")
        return
    print("Would you like to read the newly appended file? [Y/N] ", end="")
    readcommand = input()
    if readcommand == "Y": read()
    elif readcommand == "N": return
    else:
        "Incorrect input, returning."
    return

def create():
    try:
        file = open(readfile, "x")
        print(f"The file \"{readfile}\" has been created.")
    except:
        print("Error: This file already exists.")
    return

if __name__ == '__main__':
    print("File Handling with Python")
    while True:
        print("\nMenu:\n"
        "1. Definitions\n"
        "2. Read a File\n"
        "3. Append to a File\n"
        "4. Write to a File\n"
        "5. Create a File\n"
        "6. Quit\n"
        "Input: ", end="")
        command = input()

        if command == "1": definitions()
        elif command == "2":
            print("Filename?: ")
            readfile = input()
            read()
        elif command == "3":
            print("Filename?: ")
            readfile = input()
            append()
        elif command == "4":
            print("Filename?: ")
            readfile = input()
            write()
        elif command == "5":
            print("Filename?: ")
            readfile = input()
            create()
        elif command == "6": break
        else: print("Incorrect input. Please try again.")
    print("Program ending...")
    exit